﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;


namespace CDControlador
{
    public class ConProducto
    {
        private ConDB conexion = new ConDB();
        private MySqlDataReader leer;

        public MySqlDataReader MostrarProducto()
        {
            string sql = "SELECT * FROM Producto";
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        /*public MySqlDataReader Mostrar_datos_usuario(String user, String email, String telefono)
        {
            String sql = "SELECT * FROM USUARIO WHERE USER = '" + user + "'";
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }
        */
    }
}
