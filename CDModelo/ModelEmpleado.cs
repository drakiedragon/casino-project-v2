﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using CDControlador;
using MySql.Data.MySqlClient;

namespace CDModelo
{
    public class ModelEmpleado
    {
         private ConTrabajor objDato = new ConTrabajor();

        private int _id_usuario;
        private String _nombre_usuario;
        private String _nickname;
        private String _pass;
        private String _email;
        private String _direccion;
        private String _telefono;

        public int Id_usuario
        {
            set { _id_usuario = value; }
            get { return _id_usuario; }
        }
        public String Nombre_usuario
        {
            set { _nombre_usuario = value; }
            get { return _nombre_usuario; }
        }
        public String Nickname
        {
            set { _nickname = value; }
            get { return _nickname; }
        }
        public String Pass
        {
            set { _pass = value; }
            get { return _pass; }
        }
        public String email
        {
            set { _email = value; }
            get { return _email; }
        }
        public String Direccion
        {
            set { _direccion = value; }
            get { return _direccion; }
        }
        public String Telefono
        {
            set { _telefono = value; }
            get { return _telefono; }
        }

        public MySqlDataReader IniciarSesion()
        {
            MySqlDataReader loguear;
            loguear = objDato.IniciarSesion(Nickname,Pass);

            return loguear;
        }
        
        /*public MySqlDataReader Mostrar_datos_usuario()
        {
            MySqlDataReader mostrar_nombre;
            mostrar_nombre = objDato.Mostrar_datos_usuario(Nombre_usuario, email, Telefono);
            return mostrar_nombre;

        }
        */
    }
}

