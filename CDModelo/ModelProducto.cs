﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using CDControlador;
using MySql.Data.MySqlClient;

namespace CDModelo
{
    public class ModelProducto
    {
        private ConProducto objProducto = new ConProducto();

        private string _codigoProducto;
        private string _nombreProducto;
		private int _stockProducto;
		private int _idCategoria;

        public string codigoProducto
        {
            set { _codigoProducto = value; }
            get { return _codigoProducto; }
        }
		public string nombreProducto
        {
            set { _nombreProducto = value; }
            get { return _nombreProducto; }
        }
		public int stockProducto
        {
            set { _stockProducto = value; }
            get { return _stockProducto; }
        }
		public int idCategoria
        {
            set { _idCategoria = value; }
            get { return _idCategoria; }
        }
        

        public MySqlDataReader ListarProductos()
        {
            MySqlDataReader listar;
            listar = objProducto.MostrarProducto();

            return listar;
        }
        
        /*public MySqlDataReader Mostrar_datos_usuario()
        {
            MySqlDataReader mostrar_nombre;
            mostrar_nombre = objDato.Mostrar_datos_usuario(Nombre_usuario, email, Telefono);
            return mostrar_nombre;

        }
        */
    }
}

