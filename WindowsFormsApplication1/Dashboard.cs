﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CDControlador;
using CDModelo;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Dashboard : Form
    {

        double timer = 0;
        public Dashboard()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenVentas.Height!=0)
            {
                contentMenuHiddenVentas.Height = 0;
            }else{
                contentMenuHiddenProductos.Height = 0;
                contentMenuHiddenCategorias.Height = 0;
                contentMenuHiddenInventario.Height = 0;
                contentMenuHiddenProveedores.Height = 0;
                contentMenuHiddenUsuarios.Height = 0;
                timer1.Enabled = true;


            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenProductos.Height != 0)
            {
                contentMenuHiddenProductos.Height = 0;
            }
            else
            {
                contentMenuHiddenVentas.Height = 0;
                contentMenuHiddenCategorias.Height = 0;
                contentMenuHiddenInventario.Height = 0;
                contentMenuHiddenProveedores.Height = 0;
                contentMenuHiddenUsuarios.Height = 0;
                timer2.Enabled = true;
            }

            
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenInventario.Height != 0)
            {
                contentMenuHiddenInventario.Height = 0;
            }
            else
            {
                contentMenuHiddenVentas.Height = 0;
                contentMenuHiddenProductos.Height = 0;
                contentMenuHiddenCategorias.Height = 0;
                contentMenuHiddenProveedores.Height = 0;
                contentMenuHiddenUsuarios.Height = 0;
                timer4.Enabled = true;
            }
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenProveedores.Height != 0)
            {
                contentMenuHiddenProveedores.Height = 0;
            }
            else
            {
                contentMenuHiddenVentas.Height = 0;
                contentMenuHiddenProductos.Height = 0;
                contentMenuHiddenCategorias.Height = 0;
                contentMenuHiddenInventario.Height = 0;
                contentMenuHiddenUsuarios.Height = 0;
                timer5.Enabled = true;
            }
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenUsuarios.Height != 0)
            {
                contentMenuHiddenUsuarios.Height = 0;
            }
            else
            {
                contentMenuHiddenVentas.Height = 0;
                contentMenuHiddenProductos.Height = 0;
                contentMenuHiddenCategorias.Height = 0;
                contentMenuHiddenInventario.Height = 0;
                contentMenuHiddenProveedores.Height = 0;
                timer6.Enabled = true;
            }
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            if (contentMenuHiddenCategorias.Height != 0)
            {
                contentMenuHiddenCategorias.Height = 0;
            }
            else
            {
                contentMenuHiddenVentas.Height = 0;
                contentMenuHiddenProductos.Height = 0;
                contentMenuHiddenInventario.Height = 0;
                contentMenuHiddenProveedores.Height = 0;
                contentMenuHiddenUsuarios.Height = 0;
                timer3.Enabled = true;
            }
        }

        private void btnAgregarProductos_Click(object sender, EventArgs e)
        {
            //contentMenuControls.Controls.Clear();
            //AgregarProducto agregarProducto = new AgregarProducto();
            //agregarProducto.Dock = DockStyle.Fill;
            //contentMenuControls.Controls.Add(agregarProducto);
        }

        private void btnListarProductos_Click(object sender, EventArgs e)
        {
            contentMenuControls.Controls.Clear();
            //ListarProductos listarProducto = new ListarProductos();
            //listarProducto.Dock = DockStyle.Fill;
            //contentMenuControls.Controls.Add(listarProducto);
            ModelProducto objProducto = new ModelProducto();
            MySqlDataReader mostrarProducto;
            mostrarProducto = objProducto.ListarProductos();
            int contador = 0;
            contentMenuControls.Hide();
            while (mostrarProducto.Read())
            {

            
                
                    Create_Cardview(contador, mostrarProducto.GetString(0), mostrarProducto.GetString(1));
                    contador = contador + 1;

               

            }
            contentMenuControls.Show();


        }

        private void button6_Click(object sender, EventArgs e)
        {
            contentMenuControls.Controls.Clear();
//EditarProducto editarProducto = new EditarProducto();
  //          editarProducto.Dock = DockStyle.Fill;
           // contentMenuControls.Controls.Add(editarProducto);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenVentas.Height = Convert.ToInt32(timer);
            if (timer >= 156)
            {
                timer1.Enabled = false;
                timer = 0;
            }

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenProductos.Height = Convert.ToInt32(timer);
            if (timer >= 208)
            {
                timer2.Enabled = false;
                timer = 0;
            }

        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenCategorias.Height = Convert.ToInt32(timer);
            if (timer >= 208)
            {
                timer3.Enabled = false;
                timer = 0;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenInventario.Height = Convert.ToInt32(timer);
            if (timer >= 104)
            {
                timer4.Enabled = false;
                timer = 0;
            }
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenProveedores.Height = Convert.ToInt32(timer);
            if (timer >= 208)
            {
                timer5.Enabled = false;
                timer = 0;
            }
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            timer = timer + 7;
            contentMenuHiddenUsuarios.Height = Convert.ToInt32(timer);
            if (timer >= 208)
            {
                timer6.Enabled = false;
                timer = 0;
            }
        }

        private void btnPrincipal_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void Create_Cardview(int contador, string codigoProducto, string nombreProducto)
        {
            Panel panelObjetoProducto = new Panel();
            Panel panelLabelCodigoProducto = new Panel();
            Panel panelLabelNombreProducto = new Panel();
            Panel panelSeparador = new Panel();
            Label labelCodigoProducto = new Label();
            Label labelNombreProducto = new Label();
            Button btnEditar = new Button();

            
            contentMenuControls.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);

            panelObjetoProducto.Name = "panelObjetoProducto" + contador;
            panelObjetoProducto.Dock = DockStyle.Top;
            panelObjetoProducto.SetBounds(0, 0, 0, 80);
           

            panelLabelCodigoProducto.Name = "panelLabelCodigoProducto" + contador;
            panelLabelCodigoProducto.Dock = DockStyle.Top;
            panelLabelCodigoProducto.SetBounds(0, 0, 0, 30);
            panelLabelCodigoProducto.BackColor = Color.FromArgb(65, 105, 231);

            panelLabelNombreProducto.Name = "panelLabelNombreProducto" + contador;
            panelLabelNombreProducto.Dock = DockStyle.Top;
            panelLabelNombreProducto.SetBounds(0, 0, 0, 40);
            panelLabelNombreProducto.BackColor = Color.White;

            panelSeparador.Name = "panelSeparador" + contador;
            panelSeparador.Dock = DockStyle.Top;
            panelSeparador.SetBounds(0, 0, 0, 10);
            panelSeparador.BackColor = Color.FromArgb(26, 40, 86);

            labelCodigoProducto.Name = "labelCodigoProducto" + contador;
            labelCodigoProducto.Text = "Codigo Producto:    " + codigoProducto;
            labelCodigoProducto.AutoSize = true;
            labelCodigoProducto.Font = new Font("Bahnschrift SemiCondensed", 15, FontStyle.Bold);
            labelCodigoProducto.ForeColor = Color.White;
            labelCodigoProducto.Dock = DockStyle.Left;

            labelNombreProducto.Name = "labelNombreProducto" + contador;
            labelNombreProducto.Text = "Nombre Producto:    " + nombreProducto;
            labelNombreProducto.AutoSize = true;
            labelNombreProducto.Font = new Font("Bahnschrift SemiCondensed", 20, FontStyle.Bold);
            labelNombreProducto.ForeColor = Color.Black;
            labelCodigoProducto.Dock = DockStyle.Left;

            btnEditar.Name = "btnEditar" + contador;
            btnEditar.Text = "Editar";
            btnEditar.BackColor = Color.FromArgb(237, 189, 18);
            btnEditar.FlatStyle = FlatStyle.Flat;
            btnEditar.FlatAppearance.BorderSize = 0;
            btnEditar.FlatAppearance.MouseOverBackColor = Color.FromArgb(255, 255, 92);
            btnEditar.FlatAppearance.MouseOverBackColor = Color.FromArgb(216, 211, 0);
            btnEditar.Font = new Font("Bahnschrift SemiCondensed", 15, FontStyle.Bold);
            btnEditar.ForeColor = Color.White;
            btnEditar.Dock = DockStyle.Right;
            btnEditar.Click += new EventHandler(btneditar_Click);



            panelLabelNombreProducto.Controls.Add(btnEditar);
            panelLabelNombreProducto.Controls.Add(labelNombreProducto);
            panelLabelCodigoProducto.Controls.Add(labelCodigoProducto);
            panelObjetoProducto.Controls.Add(panelLabelNombreProducto);
            panelObjetoProducto.Controls.Add(panelLabelCodigoProducto);
            contentMenuControls.Controls.Add(panelObjetoProducto);
            contentMenuControls.Controls.Add(panelSeparador);
            
        }
        private void btneditar_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Hiciste Click y funciono!!");
        }
    }
}