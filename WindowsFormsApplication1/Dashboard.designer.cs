﻿namespace WindowsFormsApplication1
{
    partial class Dashboard
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.timer6 = new System.Windows.Forms.Timer(this.components);
            this.contentMenuControls = new CVistas.PanelDB();
            this.menuAccount = new CVistas.PanelDB();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnConfigCuenta = new System.Windows.Forms.Button();
            this.contentMenu = new CVistas.PanelDB();
            this.contentBtnUsuarios = new CVistas.PanelDB();
            this.contentMenuHiddenUsuarios = new CVistas.PanelDB();
            this.btnEliminarUsuario = new System.Windows.Forms.Button();
            this.btnEditarUsuario = new System.Windows.Forms.Button();
            this.btnListarUsuarios = new System.Windows.Forms.Button();
            this.btnAgregarUsuarios = new System.Windows.Forms.Button();
            this.btnUsuarios = new System.Windows.Forms.Button();
            this.contentBtnProveedores = new CVistas.PanelDB();
            this.contentMenuHiddenProveedores = new CVistas.PanelDB();
            this.btnEliminarProveedor = new System.Windows.Forms.Button();
            this.btnEditarProveedor = new System.Windows.Forms.Button();
            this.btnListarProveedores = new System.Windows.Forms.Button();
            this.btnAgregarProveedor = new System.Windows.Forms.Button();
            this.btnProveedores = new System.Windows.Forms.Button();
            this.contentBtnInventario = new CVistas.PanelDB();
            this.contentMenuHiddenInventario = new CVistas.PanelDB();
            this.btnListarProductosInventario = new System.Windows.Forms.Button();
            this.btnSuministrarProductos = new System.Windows.Forms.Button();
            this.btnInventario = new System.Windows.Forms.Button();
            this.contentBtnCategorias = new CVistas.PanelDB();
            this.contentMenuHiddenCategorias = new CVistas.PanelDB();
            this.btnEliminarCategoría = new System.Windows.Forms.Button();
            this.btnEditarCategoría = new System.Windows.Forms.Button();
            this.btnListarCategorías = new System.Windows.Forms.Button();
            this.btnAgregarCategoría = new System.Windows.Forms.Button();
            this.btnCategorias = new System.Windows.Forms.Button();
            this.contentBtnProductos = new CVistas.PanelDB();
            this.contentMenuHiddenProductos = new CVistas.PanelDB();
            this.btnEliminarProducto = new System.Windows.Forms.Button();
            this.btnEditarProducto = new System.Windows.Forms.Button();
            this.btnListarProductos = new System.Windows.Forms.Button();
            this.btnAgregarProductos = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.contentBtnVentas = new CVistas.PanelDB();
            this.contentMenuHiddenVentas = new CVistas.PanelDB();
            this.btnEliminarVentas = new System.Windows.Forms.Button();
            this.btnEditarVentas = new System.Windows.Forms.Button();
            this.btnListarVentas = new System.Windows.Forms.Button();
            this.btnVentas = new System.Windows.Forms.Button();
            this.contentBtnInicio = new CVistas.PanelDB();
            this.btnPrincipal = new System.Windows.Forms.Button();
            this.headerContent = new CVistas.PanelDB();
            this.panelDB1 = new CVistas.PanelDB();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contentUserInfo = new CVistas.PanelDB();
            this.contentUserData = new CVistas.PanelDB();
            this.nombreUsuario = new System.Windows.Forms.Label();
            this.titleBienvenida = new System.Windows.Forms.Label();
            this.contentImgUser = new CVistas.PanelDB();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contentMenuControls.SuspendLayout();
            this.menuAccount.SuspendLayout();
            this.contentMenu.SuspendLayout();
            this.contentBtnUsuarios.SuspendLayout();
            this.contentMenuHiddenUsuarios.SuspendLayout();
            this.contentBtnProveedores.SuspendLayout();
            this.contentMenuHiddenProveedores.SuspendLayout();
            this.contentBtnInventario.SuspendLayout();
            this.contentMenuHiddenInventario.SuspendLayout();
            this.contentBtnCategorias.SuspendLayout();
            this.contentMenuHiddenCategorias.SuspendLayout();
            this.contentBtnProductos.SuspendLayout();
            this.contentMenuHiddenProductos.SuspendLayout();
            this.contentBtnVentas.SuspendLayout();
            this.contentMenuHiddenVentas.SuspendLayout();
            this.contentBtnInicio.SuspendLayout();
            this.headerContent.SuspendLayout();
            this.panelDB1.SuspendLayout();
            this.contentUserInfo.SuspendLayout();
            this.contentUserData.SuspendLayout();
            this.contentImgUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 1;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Interval = 1;
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // timer5
            // 
            this.timer5.Interval = 1;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // timer6
            // 
            this.timer6.Interval = 1;
            this.timer6.Tick += new System.EventHandler(this.timer6_Tick);
            // 
            // contentMenuControls
            // 
            this.contentMenuControls.AutoScroll = true;
            this.contentMenuControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(40)))), ((int)(((byte)(86)))));
            this.contentMenuControls.Controls.Add(this.menuAccount);
            this.contentMenuControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentMenuControls.Location = new System.Drawing.Point(374, 94);
            this.contentMenuControls.Name = "contentMenuControls";
            this.contentMenuControls.Size = new System.Drawing.Size(563, 434);
            this.contentMenuControls.TabIndex = 2;
            // 
            // menuAccount
            // 
            this.menuAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.menuAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(95)))), ((int)(((byte)(193)))));
            this.menuAccount.Controls.Add(this.btnLogout);
            this.menuAccount.Controls.Add(this.btnConfigCuenta);
            this.menuAccount.Location = new System.Drawing.Point(281, 0);
            this.menuAccount.Name = "menuAccount";
            this.menuAccount.Size = new System.Drawing.Size(284, 0);
            this.menuAccount.TabIndex = 0;
            // 
            // btnLogout
            // 
            this.btnLogout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 10F);
            this.btnLogout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(0, 52);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(30, 10, 30, 10);
            this.btnLogout.Size = new System.Drawing.Size(284, 52);
            this.btnLogout.TabIndex = 9;
            this.btnLogout.Text = "          Cerrar sesión";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnConfigCuenta
            // 
            this.btnConfigCuenta.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnConfigCuenta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfigCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConfigCuenta.FlatAppearance.BorderSize = 0;
            this.btnConfigCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigCuenta.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 10F);
            this.btnConfigCuenta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnConfigCuenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfigCuenta.Location = new System.Drawing.Point(0, 0);
            this.btnConfigCuenta.Name = "btnConfigCuenta";
            this.btnConfigCuenta.Padding = new System.Windows.Forms.Padding(30, 10, 30, 10);
            this.btnConfigCuenta.Size = new System.Drawing.Size(284, 52);
            this.btnConfigCuenta.TabIndex = 8;
            this.btnConfigCuenta.Text = "          Configurar cuenta";
            this.btnConfigCuenta.UseVisualStyleBackColor = true;
            // 
            // contentMenu
            // 
            this.contentMenu.AutoScroll = true;
            this.contentMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(56)))), ((int)(((byte)(92)))));
            this.contentMenu.Controls.Add(this.contentBtnUsuarios);
            this.contentMenu.Controls.Add(this.contentBtnProveedores);
            this.contentMenu.Controls.Add(this.contentBtnInventario);
            this.contentMenu.Controls.Add(this.contentBtnCategorias);
            this.contentMenu.Controls.Add(this.contentBtnProductos);
            this.contentMenu.Controls.Add(this.contentBtnVentas);
            this.contentMenu.Controls.Add(this.contentBtnInicio);
            this.contentMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.contentMenu.Location = new System.Drawing.Point(0, 94);
            this.contentMenu.Name = "contentMenu";
            this.contentMenu.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.contentMenu.Size = new System.Drawing.Size(374, 434);
            this.contentMenu.TabIndex = 1;
            // 
            // contentBtnUsuarios
            // 
            this.contentBtnUsuarios.AutoSize = true;
            this.contentBtnUsuarios.Controls.Add(this.contentMenuHiddenUsuarios);
            this.contentBtnUsuarios.Controls.Add(this.btnUsuarios);
            this.contentBtnUsuarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnUsuarios.Location = new System.Drawing.Point(0, 352);
            this.contentBtnUsuarios.Name = "contentBtnUsuarios";
            this.contentBtnUsuarios.Size = new System.Drawing.Size(374, 52);
            this.contentBtnUsuarios.TabIndex = 15;
            // 
            // contentMenuHiddenUsuarios
            // 
            this.contentMenuHiddenUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenUsuarios.Controls.Add(this.btnEliminarUsuario);
            this.contentMenuHiddenUsuarios.Controls.Add(this.btnEditarUsuario);
            this.contentMenuHiddenUsuarios.Controls.Add(this.btnListarUsuarios);
            this.contentMenuHiddenUsuarios.Controls.Add(this.btnAgregarUsuarios);
            this.contentMenuHiddenUsuarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenUsuarios.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenUsuarios.Name = "contentMenuHiddenUsuarios";
            this.contentMenuHiddenUsuarios.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenUsuarios.TabIndex = 19;
            // 
            // btnEliminarUsuario
            // 
            this.btnEliminarUsuario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminarUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarUsuario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEliminarUsuario.FlatAppearance.BorderSize = 0;
            this.btnEliminarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarUsuario.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEliminarUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEliminarUsuario.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_delete_sweep_white_18dp;
            this.btnEliminarUsuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarUsuario.Location = new System.Drawing.Point(0, 156);
            this.btnEliminarUsuario.Name = "btnEliminarUsuario";
            this.btnEliminarUsuario.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEliminarUsuario.Size = new System.Drawing.Size(374, 52);
            this.btnEliminarUsuario.TabIndex = 21;
            this.btnEliminarUsuario.Text = "          ELIMINAR USUARIO";
            this.btnEliminarUsuario.UseVisualStyleBackColor = true;
            // 
            // btnEditarUsuario
            // 
            this.btnEditarUsuario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditarUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarUsuario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditarUsuario.FlatAppearance.BorderSize = 0;
            this.btnEditarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarUsuario.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEditarUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEditarUsuario.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_create_white_18dp;
            this.btnEditarUsuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarUsuario.Location = new System.Drawing.Point(0, 104);
            this.btnEditarUsuario.Name = "btnEditarUsuario";
            this.btnEditarUsuario.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEditarUsuario.Size = new System.Drawing.Size(374, 52);
            this.btnEditarUsuario.TabIndex = 20;
            this.btnEditarUsuario.Text = "          EDITAR USUARIO";
            this.btnEditarUsuario.UseVisualStyleBackColor = true;
            // 
            // btnListarUsuarios
            // 
            this.btnListarUsuarios.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarUsuarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarUsuarios.FlatAppearance.BorderSize = 0;
            this.btnListarUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarUsuarios.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarUsuarios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarUsuarios.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarUsuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarUsuarios.Location = new System.Drawing.Point(0, 52);
            this.btnListarUsuarios.Name = "btnListarUsuarios";
            this.btnListarUsuarios.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarUsuarios.Size = new System.Drawing.Size(374, 52);
            this.btnListarUsuarios.TabIndex = 19;
            this.btnListarUsuarios.Text = "          LISTAR USUARIOS";
            this.btnListarUsuarios.UseVisualStyleBackColor = true;
            // 
            // btnAgregarUsuarios
            // 
            this.btnAgregarUsuarios.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregarUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarUsuarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarUsuarios.FlatAppearance.BorderSize = 0;
            this.btnAgregarUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarUsuarios.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnAgregarUsuarios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnAgregarUsuarios.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_add_circle_outline_white_18dp;
            this.btnAgregarUsuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarUsuarios.Location = new System.Drawing.Point(0, 0);
            this.btnAgregarUsuarios.Name = "btnAgregarUsuarios";
            this.btnAgregarUsuarios.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnAgregarUsuarios.Size = new System.Drawing.Size(374, 52);
            this.btnAgregarUsuarios.TabIndex = 18;
            this.btnAgregarUsuarios.Text = "          AGREGAR USUARIO";
            this.btnAgregarUsuarios.UseVisualStyleBackColor = true;
            // 
            // btnUsuarios
            // 
            this.btnUsuarios.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUsuarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUsuarios.FlatAppearance.BorderSize = 0;
            this.btnUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsuarios.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnUsuarios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnUsuarios.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_group_white_18dp;
            this.btnUsuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsuarios.Location = new System.Drawing.Point(0, 0);
            this.btnUsuarios.Name = "btnUsuarios";
            this.btnUsuarios.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnUsuarios.Size = new System.Drawing.Size(374, 52);
            this.btnUsuarios.TabIndex = 20;
            this.btnUsuarios.Text = "USUARIOS";
            this.btnUsuarios.UseVisualStyleBackColor = true;
            this.btnUsuarios.Click += new System.EventHandler(this.btnUsuarios_Click);
            // 
            // contentBtnProveedores
            // 
            this.contentBtnProveedores.AutoSize = true;
            this.contentBtnProveedores.Controls.Add(this.contentMenuHiddenProveedores);
            this.contentBtnProveedores.Controls.Add(this.btnProveedores);
            this.contentBtnProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnProveedores.Location = new System.Drawing.Point(0, 300);
            this.contentBtnProveedores.Name = "contentBtnProveedores";
            this.contentBtnProveedores.Size = new System.Drawing.Size(374, 52);
            this.contentBtnProveedores.TabIndex = 14;
            // 
            // contentMenuHiddenProveedores
            // 
            this.contentMenuHiddenProveedores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenProveedores.Controls.Add(this.btnEliminarProveedor);
            this.contentMenuHiddenProveedores.Controls.Add(this.btnEditarProveedor);
            this.contentMenuHiddenProveedores.Controls.Add(this.btnListarProveedores);
            this.contentMenuHiddenProveedores.Controls.Add(this.btnAgregarProveedor);
            this.contentMenuHiddenProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenProveedores.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenProveedores.Name = "contentMenuHiddenProveedores";
            this.contentMenuHiddenProveedores.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenProveedores.TabIndex = 19;
            // 
            // btnEliminarProveedor
            // 
            this.btnEliminarProveedor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEliminarProveedor.FlatAppearance.BorderSize = 0;
            this.btnEliminarProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarProveedor.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEliminarProveedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEliminarProveedor.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_delete_sweep_white_18dp;
            this.btnEliminarProveedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarProveedor.Location = new System.Drawing.Point(0, 156);
            this.btnEliminarProveedor.Name = "btnEliminarProveedor";
            this.btnEliminarProveedor.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEliminarProveedor.Size = new System.Drawing.Size(374, 52);
            this.btnEliminarProveedor.TabIndex = 21;
            this.btnEliminarProveedor.Text = "          ELIMINAR PROVEEDOR";
            this.btnEliminarProveedor.UseVisualStyleBackColor = true;
            // 
            // btnEditarProveedor
            // 
            this.btnEditarProveedor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditarProveedor.FlatAppearance.BorderSize = 0;
            this.btnEditarProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarProveedor.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEditarProveedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEditarProveedor.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_create_white_18dp;
            this.btnEditarProveedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarProveedor.Location = new System.Drawing.Point(0, 104);
            this.btnEditarProveedor.Name = "btnEditarProveedor";
            this.btnEditarProveedor.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEditarProveedor.Size = new System.Drawing.Size(374, 52);
            this.btnEditarProveedor.TabIndex = 20;
            this.btnEditarProveedor.Text = "          EDITAR PROVEEDOR";
            this.btnEditarProveedor.UseVisualStyleBackColor = true;
            // 
            // btnListarProveedores
            // 
            this.btnListarProveedores.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarProveedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarProveedores.FlatAppearance.BorderSize = 0;
            this.btnListarProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarProveedores.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarProveedores.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarProveedores.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarProveedores.Location = new System.Drawing.Point(0, 52);
            this.btnListarProveedores.Name = "btnListarProveedores";
            this.btnListarProveedores.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarProveedores.Size = new System.Drawing.Size(374, 52);
            this.btnListarProveedores.TabIndex = 19;
            this.btnListarProveedores.Text = "          LISTAR PROVEEDORES";
            this.btnListarProveedores.UseVisualStyleBackColor = true;
            // 
            // btnAgregarProveedor
            // 
            this.btnAgregarProveedor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregarProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarProveedor.FlatAppearance.BorderSize = 0;
            this.btnAgregarProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProveedor.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnAgregarProveedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnAgregarProveedor.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_add_circle_outline_white_18dp;
            this.btnAgregarProveedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProveedor.Location = new System.Drawing.Point(0, 0);
            this.btnAgregarProveedor.Name = "btnAgregarProveedor";
            this.btnAgregarProveedor.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnAgregarProveedor.Size = new System.Drawing.Size(374, 52);
            this.btnAgregarProveedor.TabIndex = 18;
            this.btnAgregarProveedor.Text = "          AGREGAR PROVEEDOR";
            this.btnAgregarProveedor.UseVisualStyleBackColor = true;
            // 
            // btnProveedores
            // 
            this.btnProveedores.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnProveedores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProveedores.FlatAppearance.BorderSize = 0;
            this.btnProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProveedores.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnProveedores.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnProveedores.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_local_shipping_white_18dp;
            this.btnProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProveedores.Location = new System.Drawing.Point(0, 0);
            this.btnProveedores.Name = "btnProveedores";
            this.btnProveedores.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnProveedores.Size = new System.Drawing.Size(374, 52);
            this.btnProveedores.TabIndex = 20;
            this.btnProveedores.Text = "PROVEEDORES";
            this.btnProveedores.UseVisualStyleBackColor = true;
            this.btnProveedores.Click += new System.EventHandler(this.btnProveedores_Click);
            // 
            // contentBtnInventario
            // 
            this.contentBtnInventario.AutoSize = true;
            this.contentBtnInventario.Controls.Add(this.contentMenuHiddenInventario);
            this.contentBtnInventario.Controls.Add(this.btnInventario);
            this.contentBtnInventario.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnInventario.Location = new System.Drawing.Point(0, 248);
            this.contentBtnInventario.Name = "contentBtnInventario";
            this.contentBtnInventario.Size = new System.Drawing.Size(374, 52);
            this.contentBtnInventario.TabIndex = 13;
            // 
            // contentMenuHiddenInventario
            // 
            this.contentMenuHiddenInventario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenInventario.Controls.Add(this.btnListarProductosInventario);
            this.contentMenuHiddenInventario.Controls.Add(this.btnSuministrarProductos);
            this.contentMenuHiddenInventario.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenInventario.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenInventario.Name = "contentMenuHiddenInventario";
            this.contentMenuHiddenInventario.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenInventario.TabIndex = 19;
            // 
            // btnListarProductosInventario
            // 
            this.btnListarProductosInventario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarProductosInventario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarProductosInventario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarProductosInventario.FlatAppearance.BorderSize = 0;
            this.btnListarProductosInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarProductosInventario.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarProductosInventario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarProductosInventario.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarProductosInventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarProductosInventario.Location = new System.Drawing.Point(0, 52);
            this.btnListarProductosInventario.Name = "btnListarProductosInventario";
            this.btnListarProductosInventario.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarProductosInventario.Size = new System.Drawing.Size(374, 52);
            this.btnListarProductosInventario.TabIndex = 19;
            this.btnListarProductosInventario.Text = "          LISTAR PRODUCTOS INVENTARIO";
            this.btnListarProductosInventario.UseVisualStyleBackColor = true;
            // 
            // btnSuministrarProductos
            // 
            this.btnSuministrarProductos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSuministrarProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSuministrarProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSuministrarProductos.FlatAppearance.BorderSize = 0;
            this.btnSuministrarProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuministrarProductos.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnSuministrarProductos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnSuministrarProductos.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_add_circle_outline_white_18dp;
            this.btnSuministrarProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSuministrarProductos.Location = new System.Drawing.Point(0, 0);
            this.btnSuministrarProductos.Name = "btnSuministrarProductos";
            this.btnSuministrarProductos.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnSuministrarProductos.Size = new System.Drawing.Size(374, 52);
            this.btnSuministrarProductos.TabIndex = 18;
            this.btnSuministrarProductos.Text = "          SUMINISTRAR PRODUCTOS";
            this.btnSuministrarProductos.UseVisualStyleBackColor = true;
            // 
            // btnInventario
            // 
            this.btnInventario.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnInventario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInventario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInventario.FlatAppearance.BorderSize = 0;
            this.btnInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInventario.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnInventario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnInventario.Image = global::WindowsFormsApplication1.Properties.Resources.outline_assignment_white_18dp;
            this.btnInventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInventario.Location = new System.Drawing.Point(0, 0);
            this.btnInventario.Name = "btnInventario";
            this.btnInventario.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnInventario.Size = new System.Drawing.Size(374, 52);
            this.btnInventario.TabIndex = 20;
            this.btnInventario.Text = "INVENTARIO";
            this.btnInventario.UseVisualStyleBackColor = true;
            this.btnInventario.Click += new System.EventHandler(this.btnInventario_Click);
            // 
            // contentBtnCategorias
            // 
            this.contentBtnCategorias.AutoSize = true;
            this.contentBtnCategorias.Controls.Add(this.contentMenuHiddenCategorias);
            this.contentBtnCategorias.Controls.Add(this.btnCategorias);
            this.contentBtnCategorias.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnCategorias.Location = new System.Drawing.Point(0, 196);
            this.contentBtnCategorias.Name = "contentBtnCategorias";
            this.contentBtnCategorias.Size = new System.Drawing.Size(374, 52);
            this.contentBtnCategorias.TabIndex = 16;
            // 
            // contentMenuHiddenCategorias
            // 
            this.contentMenuHiddenCategorias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenCategorias.Controls.Add(this.btnEliminarCategoría);
            this.contentMenuHiddenCategorias.Controls.Add(this.btnEditarCategoría);
            this.contentMenuHiddenCategorias.Controls.Add(this.btnListarCategorías);
            this.contentMenuHiddenCategorias.Controls.Add(this.btnAgregarCategoría);
            this.contentMenuHiddenCategorias.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenCategorias.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenCategorias.Name = "contentMenuHiddenCategorias";
            this.contentMenuHiddenCategorias.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenCategorias.TabIndex = 19;
            // 
            // btnEliminarCategoría
            // 
            this.btnEliminarCategoría.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminarCategoría.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarCategoría.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEliminarCategoría.FlatAppearance.BorderSize = 0;
            this.btnEliminarCategoría.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarCategoría.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEliminarCategoría.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEliminarCategoría.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_delete_sweep_white_18dp;
            this.btnEliminarCategoría.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarCategoría.Location = new System.Drawing.Point(0, 156);
            this.btnEliminarCategoría.Name = "btnEliminarCategoría";
            this.btnEliminarCategoría.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEliminarCategoría.Size = new System.Drawing.Size(374, 52);
            this.btnEliminarCategoría.TabIndex = 21;
            this.btnEliminarCategoría.Text = "          ELIMINAR CATEGORÍA";
            this.btnEliminarCategoría.UseVisualStyleBackColor = true;
            // 
            // btnEditarCategoría
            // 
            this.btnEditarCategoría.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditarCategoría.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarCategoría.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditarCategoría.FlatAppearance.BorderSize = 0;
            this.btnEditarCategoría.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarCategoría.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEditarCategoría.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEditarCategoría.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_create_white_18dp;
            this.btnEditarCategoría.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarCategoría.Location = new System.Drawing.Point(0, 104);
            this.btnEditarCategoría.Name = "btnEditarCategoría";
            this.btnEditarCategoría.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEditarCategoría.Size = new System.Drawing.Size(374, 52);
            this.btnEditarCategoría.TabIndex = 20;
            this.btnEditarCategoría.Text = "          EDITAR CATEGORÍA";
            this.btnEditarCategoría.UseVisualStyleBackColor = true;
            // 
            // btnListarCategorías
            // 
            this.btnListarCategorías.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarCategorías.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarCategorías.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarCategorías.FlatAppearance.BorderSize = 0;
            this.btnListarCategorías.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarCategorías.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarCategorías.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarCategorías.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarCategorías.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarCategorías.Location = new System.Drawing.Point(0, 52);
            this.btnListarCategorías.Name = "btnListarCategorías";
            this.btnListarCategorías.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarCategorías.Size = new System.Drawing.Size(374, 52);
            this.btnListarCategorías.TabIndex = 19;
            this.btnListarCategorías.Text = "          LISTAR CATEGORÍAS";
            this.btnListarCategorías.UseVisualStyleBackColor = true;
            // 
            // btnAgregarCategoría
            // 
            this.btnAgregarCategoría.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregarCategoría.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarCategoría.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarCategoría.FlatAppearance.BorderSize = 0;
            this.btnAgregarCategoría.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarCategoría.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnAgregarCategoría.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnAgregarCategoría.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_add_circle_outline_white_18dp;
            this.btnAgregarCategoría.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarCategoría.Location = new System.Drawing.Point(0, 0);
            this.btnAgregarCategoría.Name = "btnAgregarCategoría";
            this.btnAgregarCategoría.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnAgregarCategoría.Size = new System.Drawing.Size(374, 52);
            this.btnAgregarCategoría.TabIndex = 18;
            this.btnAgregarCategoría.Text = "          AGREGAR CATEGORÍA";
            this.btnAgregarCategoría.UseVisualStyleBackColor = true;
            // 
            // btnCategorias
            // 
            this.btnCategorias.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCategorias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCategorias.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategorias.FlatAppearance.BorderSize = 0;
            this.btnCategorias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategorias.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnCategorias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnCategorias.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_local_offer_white_18dpv2;
            this.btnCategorias.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategorias.Location = new System.Drawing.Point(0, 0);
            this.btnCategorias.Name = "btnCategorias";
            this.btnCategorias.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnCategorias.Size = new System.Drawing.Size(374, 52);
            this.btnCategorias.TabIndex = 20;
            this.btnCategorias.Text = "CATEGORÍAS";
            this.btnCategorias.UseVisualStyleBackColor = true;
            this.btnCategorias.Click += new System.EventHandler(this.btnCategorias_Click);
            // 
            // contentBtnProductos
            // 
            this.contentBtnProductos.AutoSize = true;
            this.contentBtnProductos.Controls.Add(this.contentMenuHiddenProductos);
            this.contentBtnProductos.Controls.Add(this.btnProductos);
            this.contentBtnProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnProductos.Location = new System.Drawing.Point(0, 144);
            this.contentBtnProductos.Name = "contentBtnProductos";
            this.contentBtnProductos.Size = new System.Drawing.Size(374, 52);
            this.contentBtnProductos.TabIndex = 12;
            // 
            // contentMenuHiddenProductos
            // 
            this.contentMenuHiddenProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenProductos.Controls.Add(this.btnEliminarProducto);
            this.contentMenuHiddenProductos.Controls.Add(this.btnEditarProducto);
            this.contentMenuHiddenProductos.Controls.Add(this.btnListarProductos);
            this.contentMenuHiddenProductos.Controls.Add(this.btnAgregarProductos);
            this.contentMenuHiddenProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenProductos.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenProductos.Name = "contentMenuHiddenProductos";
            this.contentMenuHiddenProductos.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenProductos.TabIndex = 19;
            // 
            // btnEliminarProducto
            // 
            this.btnEliminarProducto.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEliminarProducto.FlatAppearance.BorderSize = 0;
            this.btnEliminarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarProducto.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEliminarProducto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEliminarProducto.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_delete_sweep_white_18dp;
            this.btnEliminarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarProducto.Location = new System.Drawing.Point(0, 156);
            this.btnEliminarProducto.Name = "btnEliminarProducto";
            this.btnEliminarProducto.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEliminarProducto.Size = new System.Drawing.Size(374, 52);
            this.btnEliminarProducto.TabIndex = 21;
            this.btnEliminarProducto.Text = "          ELIMINAR PRODUCTO";
            this.btnEliminarProducto.UseVisualStyleBackColor = true;
            // 
            // btnEditarProducto
            // 
            this.btnEditarProducto.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditarProducto.FlatAppearance.BorderSize = 0;
            this.btnEditarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarProducto.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEditarProducto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEditarProducto.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_create_white_18dp;
            this.btnEditarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarProducto.Location = new System.Drawing.Point(0, 104);
            this.btnEditarProducto.Name = "btnEditarProducto";
            this.btnEditarProducto.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEditarProducto.Size = new System.Drawing.Size(374, 52);
            this.btnEditarProducto.TabIndex = 20;
            this.btnEditarProducto.Text = "          EDITAR PRODUCTO";
            this.btnEditarProducto.UseVisualStyleBackColor = true;
            this.btnEditarProducto.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnListarProductos
            // 
            this.btnListarProductos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarProductos.FlatAppearance.BorderSize = 0;
            this.btnListarProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarProductos.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarProductos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarProductos.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarProductos.Location = new System.Drawing.Point(0, 52);
            this.btnListarProductos.Name = "btnListarProductos";
            this.btnListarProductos.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarProductos.Size = new System.Drawing.Size(374, 52);
            this.btnListarProductos.TabIndex = 19;
            this.btnListarProductos.Text = "          LISTAR PRODUCTOS";
            this.btnListarProductos.UseVisualStyleBackColor = true;
            this.btnListarProductos.Click += new System.EventHandler(this.btnListarProductos_Click);
            // 
            // btnAgregarProductos
            // 
            this.btnAgregarProductos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregarProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAgregarProductos.FlatAppearance.BorderSize = 0;
            this.btnAgregarProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProductos.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnAgregarProductos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnAgregarProductos.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_add_circle_outline_white_18dp;
            this.btnAgregarProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProductos.Location = new System.Drawing.Point(0, 0);
            this.btnAgregarProductos.Name = "btnAgregarProductos";
            this.btnAgregarProductos.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnAgregarProductos.Size = new System.Drawing.Size(374, 52);
            this.btnAgregarProductos.TabIndex = 18;
            this.btnAgregarProductos.Text = "          AGREGAR PRODUCTO";
            this.btnAgregarProductos.UseVisualStyleBackColor = true;
            this.btnAgregarProductos.Click += new System.EventHandler(this.btnAgregarProductos_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductos.FlatAppearance.BorderSize = 0;
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnProductos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnProductos.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_local_mall_white_18dp;
            this.btnProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductos.Location = new System.Drawing.Point(0, 0);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnProductos.Size = new System.Drawing.Size(374, 52);
            this.btnProductos.TabIndex = 20;
            this.btnProductos.Text = "PRODUCTOS";
            this.btnProductos.UseVisualStyleBackColor = true;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // contentBtnVentas
            // 
            this.contentBtnVentas.AutoSize = true;
            this.contentBtnVentas.Controls.Add(this.contentMenuHiddenVentas);
            this.contentBtnVentas.Controls.Add(this.btnVentas);
            this.contentBtnVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnVentas.Location = new System.Drawing.Point(0, 92);
            this.contentBtnVentas.Name = "contentBtnVentas";
            this.contentBtnVentas.Size = new System.Drawing.Size(374, 52);
            this.contentBtnVentas.TabIndex = 11;
            // 
            // contentMenuHiddenVentas
            // 
            this.contentMenuHiddenVentas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(92)))), ((int)(((byte)(96)))));
            this.contentMenuHiddenVentas.Controls.Add(this.btnEliminarVentas);
            this.contentMenuHiddenVentas.Controls.Add(this.btnEditarVentas);
            this.contentMenuHiddenVentas.Controls.Add(this.btnListarVentas);
            this.contentMenuHiddenVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentMenuHiddenVentas.Location = new System.Drawing.Point(0, 52);
            this.contentMenuHiddenVentas.Name = "contentMenuHiddenVentas";
            this.contentMenuHiddenVentas.Size = new System.Drawing.Size(374, 0);
            this.contentMenuHiddenVentas.TabIndex = 19;
            // 
            // btnEliminarVentas
            // 
            this.btnEliminarVentas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminarVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEliminarVentas.FlatAppearance.BorderSize = 0;
            this.btnEliminarVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarVentas.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEliminarVentas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEliminarVentas.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_delete_sweep_white_18dp;
            this.btnEliminarVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarVentas.Location = new System.Drawing.Point(0, 104);
            this.btnEliminarVentas.Name = "btnEliminarVentas";
            this.btnEliminarVentas.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEliminarVentas.Size = new System.Drawing.Size(374, 52);
            this.btnEliminarVentas.TabIndex = 21;
            this.btnEliminarVentas.Text = "          ELIMINAR VENTA";
            this.btnEliminarVentas.UseVisualStyleBackColor = true;
            // 
            // btnEditarVentas
            // 
            this.btnEditarVentas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEditarVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditarVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditarVentas.FlatAppearance.BorderSize = 0;
            this.btnEditarVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarVentas.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnEditarVentas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnEditarVentas.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_create_white_18dp;
            this.btnEditarVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarVentas.Location = new System.Drawing.Point(0, 52);
            this.btnEditarVentas.Name = "btnEditarVentas";
            this.btnEditarVentas.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnEditarVentas.Size = new System.Drawing.Size(374, 52);
            this.btnEditarVentas.TabIndex = 20;
            this.btnEditarVentas.Text = "          EDITAR VENTA";
            this.btnEditarVentas.UseVisualStyleBackColor = true;
            // 
            // btnListarVentas
            // 
            this.btnListarVentas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnListarVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListarVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnListarVentas.FlatAppearance.BorderSize = 0;
            this.btnListarVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarVentas.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnListarVentas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnListarVentas.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_dehaze_white_18dp;
            this.btnListarVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListarVentas.Location = new System.Drawing.Point(0, 0);
            this.btnListarVentas.Name = "btnListarVentas";
            this.btnListarVentas.Padding = new System.Windows.Forms.Padding(70, 10, 0, 10);
            this.btnListarVentas.Size = new System.Drawing.Size(374, 52);
            this.btnListarVentas.TabIndex = 19;
            this.btnListarVentas.Text = "          LISTAR VENTAS";
            this.btnListarVentas.UseVisualStyleBackColor = true;
            // 
            // btnVentas
            // 
            this.btnVentas.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVentas.FlatAppearance.BorderSize = 0;
            this.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentas.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnVentas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnVentas.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_shopping_cart_white_18dp;
            this.btnVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentas.Location = new System.Drawing.Point(0, 0);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnVentas.Size = new System.Drawing.Size(374, 52);
            this.btnVentas.TabIndex = 20;
            this.btnVentas.Text = "VENTAS";
            this.btnVentas.UseVisualStyleBackColor = true;
            this.btnVentas.Click += new System.EventHandler(this.button1_Click);
            // 
            // contentBtnInicio
            // 
            this.contentBtnInicio.AutoSize = true;
            this.contentBtnInicio.Controls.Add(this.btnPrincipal);
            this.contentBtnInicio.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentBtnInicio.Location = new System.Drawing.Point(0, 20);
            this.contentBtnInicio.Name = "contentBtnInicio";
            this.contentBtnInicio.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.contentBtnInicio.Size = new System.Drawing.Size(374, 72);
            this.contentBtnInicio.TabIndex = 17;
            // 
            // btnPrincipal
            // 
            this.btnPrincipal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPrincipal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrincipal.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrincipal.FlatAppearance.BorderSize = 0;
            this.btnPrincipal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrincipal.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.btnPrincipal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnPrincipal.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_home_white_18dp;
            this.btnPrincipal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrincipal.Location = new System.Drawing.Point(0, 0);
            this.btnPrincipal.Name = "btnPrincipal";
            this.btnPrincipal.Padding = new System.Windows.Forms.Padding(50, 10, 20, 10);
            this.btnPrincipal.Size = new System.Drawing.Size(374, 52);
            this.btnPrincipal.TabIndex = 20;
            this.btnPrincipal.Text = "INICIO";
            this.btnPrincipal.UseVisualStyleBackColor = true;
            this.btnPrincipal.Click += new System.EventHandler(this.btnPrincipal_Click);
            // 
            // headerContent
            // 
            this.headerContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(92)))), ((int)(((byte)(138)))));
            this.headerContent.Controls.Add(this.panelDB1);
            this.headerContent.Controls.Add(this.contentUserInfo);
            this.headerContent.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerContent.Location = new System.Drawing.Point(0, 0);
            this.headerContent.Name = "headerContent";
            this.headerContent.Size = new System.Drawing.Size(937, 94);
            this.headerContent.TabIndex = 0;
            // 
            // panelDB1
            // 
            this.panelDB1.BackColor = System.Drawing.Color.Transparent;
            this.panelDB1.Controls.Add(this.label4);
            this.panelDB1.Controls.Add(this.label3);
            this.panelDB1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDB1.Location = new System.Drawing.Point(0, 0);
            this.panelDB1.Name = "panelDB1";
            this.panelDB1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panelDB1.Size = new System.Drawing.Size(374, 94);
            this.panelDB1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(374, 41);
            this.label4.TabIndex = 1;
            this.label4.Text = "CASINO EL MONJE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(374, 33);
            this.label3.TabIndex = 0;
            this.label3.Text = "SISTEMA DE GESTIÓN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contentUserInfo
            // 
            this.contentUserInfo.AutoSize = true;
            this.contentUserInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(81)))), ((int)(((byte)(193)))));
            this.contentUserInfo.Controls.Add(this.contentUserData);
            this.contentUserInfo.Controls.Add(this.contentImgUser);
            this.contentUserInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.contentUserInfo.Location = new System.Drawing.Point(568, 0);
            this.contentUserInfo.Name = "contentUserInfo";
            this.contentUserInfo.Size = new System.Drawing.Size(369, 94);
            this.contentUserInfo.TabIndex = 0;
            // 
            // contentUserData
            // 
            this.contentUserData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(18)))));
            this.contentUserData.Controls.Add(this.nombreUsuario);
            this.contentUserData.Controls.Add(this.titleBienvenida);
            this.contentUserData.Dock = System.Windows.Forms.DockStyle.Left;
            this.contentUserData.Location = new System.Drawing.Point(85, 0);
            this.contentUserData.Name = "contentUserData";
            this.contentUserData.Size = new System.Drawing.Size(284, 94);
            this.contentUserData.TabIndex = 1;
            // 
            // nombreUsuario
            // 
            this.nombreUsuario.BackColor = System.Drawing.Color.Transparent;
            this.nombreUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nombreUsuario.Font = new System.Drawing.Font("Bahnschrift Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreUsuario.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.nombreUsuario.Location = new System.Drawing.Point(0, 31);
            this.nombreUsuario.Name = "nombreUsuario";
            this.nombreUsuario.Padding = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.nombreUsuario.Size = new System.Drawing.Size(284, 63);
            this.nombreUsuario.TabIndex = 0;
            this.nombreUsuario.Text = "Roberto Moore G";
            this.nombreUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // titleBienvenida
            // 
            this.titleBienvenida.AutoSize = true;
            this.titleBienvenida.BackColor = System.Drawing.Color.Transparent;
            this.titleBienvenida.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleBienvenida.Font = new System.Drawing.Font("Bahnschrift Light Condensed", 12.25F);
            this.titleBienvenida.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.titleBienvenida.Location = new System.Drawing.Point(0, 0);
            this.titleBienvenida.Name = "titleBienvenida";
            this.titleBienvenida.Padding = new System.Windows.Forms.Padding(10, 10, 30, 0);
            this.titleBienvenida.Size = new System.Drawing.Size(159, 31);
            this.titleBienvenida.TabIndex = 1;
            this.titleBienvenida.Text = "Bienvenido, Roberto";
            this.titleBienvenida.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contentImgUser
            // 
            this.contentImgUser.AutoSize = true;
            this.contentImgUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(18)))));
            this.contentImgUser.Controls.Add(this.pictureBox1);
            this.contentImgUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.contentImgUser.Location = new System.Drawing.Point(0, 0);
            this.contentImgUser.Name = "contentImgUser";
            this.contentImgUser.Padding = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.contentImgUser.Size = new System.Drawing.Size(85, 94);
            this.contentImgUser.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(18)))));
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.baseline_account_circle_white_48dp;
            this.pictureBox1.Location = new System.Drawing.Point(5, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 528);
            this.Controls.Add(this.contentMenuControls);
            this.Controls.Add(this.contentMenu);
            this.Controls.Add(this.headerContent);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.contentMenuControls.ResumeLayout(false);
            this.menuAccount.ResumeLayout(false);
            this.contentMenu.ResumeLayout(false);
            this.contentMenu.PerformLayout();
            this.contentBtnUsuarios.ResumeLayout(false);
            this.contentMenuHiddenUsuarios.ResumeLayout(false);
            this.contentBtnProveedores.ResumeLayout(false);
            this.contentMenuHiddenProveedores.ResumeLayout(false);
            this.contentBtnInventario.ResumeLayout(false);
            this.contentMenuHiddenInventario.ResumeLayout(false);
            this.contentBtnCategorias.ResumeLayout(false);
            this.contentMenuHiddenCategorias.ResumeLayout(false);
            this.contentBtnProductos.ResumeLayout(false);
            this.contentMenuHiddenProductos.ResumeLayout(false);
            this.contentBtnVentas.ResumeLayout(false);
            this.contentMenuHiddenVentas.ResumeLayout(false);
            this.contentBtnInicio.ResumeLayout(false);
            this.headerContent.ResumeLayout(false);
            this.headerContent.PerformLayout();
            this.panelDB1.ResumeLayout(false);
            this.contentUserInfo.ResumeLayout(false);
            this.contentUserInfo.PerformLayout();
            this.contentUserData.ResumeLayout(false);
            this.contentUserData.PerformLayout();
            this.contentImgUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CVistas.PanelDB headerContent;
        private CVistas.PanelDB contentUserInfo;
        private CVistas.PanelDB contentMenuControls;
        private CVistas.PanelDB contentImgUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CVistas.PanelDB contentUserData;
        private CVistas.PanelDB menuAccount;
        internal System.Windows.Forms.Button btnConfigCuenta;
        internal System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label nombreUsuario;
        private CVistas.PanelDB panelDB1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label titleBienvenida;
        private CVistas.PanelDB contentMenu;
        private CVistas.PanelDB contentBtnInventario;
        private CVistas.PanelDB contentMenuHiddenInventario;
        internal System.Windows.Forms.Button btnListarProductosInventario;
        internal System.Windows.Forms.Button btnSuministrarProductos;
        internal System.Windows.Forms.Button btnInventario;
        private CVistas.PanelDB contentBtnProductos;
        private CVistas.PanelDB contentMenuHiddenProductos;
        internal System.Windows.Forms.Button btnEliminarProducto;
        internal System.Windows.Forms.Button btnEditarProducto;
        internal System.Windows.Forms.Button btnListarProductos;
        internal System.Windows.Forms.Button btnAgregarProductos;
        internal System.Windows.Forms.Button btnProductos;
        private CVistas.PanelDB contentBtnVentas;
        private CVistas.PanelDB contentMenuHiddenVentas;
        internal System.Windows.Forms.Button btnEliminarVentas;
        internal System.Windows.Forms.Button btnEditarVentas;
        internal System.Windows.Forms.Button btnListarVentas;
        internal System.Windows.Forms.Button btnVentas;
        private CVistas.PanelDB contentBtnUsuarios;
        private CVistas.PanelDB contentMenuHiddenUsuarios;
        internal System.Windows.Forms.Button btnEliminarUsuario;
        internal System.Windows.Forms.Button btnEditarUsuario;
        internal System.Windows.Forms.Button btnListarUsuarios;
        internal System.Windows.Forms.Button btnAgregarUsuarios;
        internal System.Windows.Forms.Button btnUsuarios;
        private CVistas.PanelDB contentBtnProveedores;
        private CVistas.PanelDB contentMenuHiddenProveedores;
        internal System.Windows.Forms.Button btnEliminarProveedor;
        internal System.Windows.Forms.Button btnEditarProveedor;
        internal System.Windows.Forms.Button btnListarProveedores;
        internal System.Windows.Forms.Button btnAgregarProveedor;
        internal System.Windows.Forms.Button btnProveedores;
        private CVistas.PanelDB contentBtnCategorias;
        private CVistas.PanelDB contentMenuHiddenCategorias;
        internal System.Windows.Forms.Button btnListarCategorías;
        internal System.Windows.Forms.Button btnAgregarCategoría;
        internal System.Windows.Forms.Button btnCategorias;
        internal System.Windows.Forms.Button btnEliminarCategoría;
        internal System.Windows.Forms.Button btnEditarCategoría;
        private CVistas.PanelDB contentBtnInicio;
        internal System.Windows.Forms.Button btnPrincipal;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.Timer timer6;

    }
}

