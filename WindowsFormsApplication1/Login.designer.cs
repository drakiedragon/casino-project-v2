﻿namespace WindowsFormsApplication1
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelDB1 = new CVistas.PanelDB();
            this.contentInputs = new CVistas.PanelDB();
            this.contentControlsButtonLogin = new CVistas.PanelDB();
            this.panelDB8 = new CVistas.PanelDB();
            this.btnLogin = new System.Windows.Forms.Button();
            this.panelDB9 = new CVistas.PanelDB();
            this.mensajeErrorLabel = new System.Windows.Forms.Label();
            this.contentControlsContraseña = new CVistas.PanelDB();
            this.panelDB5 = new CVistas.PanelDB();
            this.inputContraseña = new System.Windows.Forms.TextBox();
            this.panelDB6 = new CVistas.PanelDB();
            this.contraseñaLabel = new System.Windows.Forms.Label();
            this.contentControlsNombreUsuario = new CVistas.PanelDB();
            this.contentInputNombre = new CVistas.PanelDB();
            this.inputNombreUsuario = new System.Windows.Forms.TextBox();
            this.contentLabelNombreU = new CVistas.PanelDB();
            this.nombreUsuarioLabel = new System.Windows.Forms.Label();
            this.panelDB2 = new CVistas.PanelDB();
            this.panelDB3 = new CVistas.PanelDB();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelDB1.SuspendLayout();
            this.contentInputs.SuspendLayout();
            this.contentControlsButtonLogin.SuspendLayout();
            this.panelDB8.SuspendLayout();
            this.panelDB9.SuspendLayout();
            this.contentControlsContraseña.SuspendLayout();
            this.panelDB5.SuspendLayout();
            this.panelDB6.SuspendLayout();
            this.contentControlsNombreUsuario.SuspendLayout();
            this.contentInputNombre.SuspendLayout();
            this.contentLabelNombreU.SuspendLayout();
            this.panelDB2.SuspendLayout();
            this.panelDB3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelDB1
            // 
            this.panelDB1.Controls.Add(this.contentInputs);
            this.panelDB1.Controls.Add(this.panelDB2);
            this.panelDB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDB1.Location = new System.Drawing.Point(0, 0);
            this.panelDB1.Name = "panelDB1";
            this.panelDB1.Size = new System.Drawing.Size(433, 427);
            this.panelDB1.TabIndex = 0;
            // 
            // contentInputs
            // 
            this.contentInputs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(56)))), ((int)(((byte)(92)))));
            this.contentInputs.Controls.Add(this.contentControlsButtonLogin);
            this.contentInputs.Controls.Add(this.contentControlsContraseña);
            this.contentInputs.Controls.Add(this.contentControlsNombreUsuario);
            this.contentInputs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentInputs.Location = new System.Drawing.Point(0, 100);
            this.contentInputs.Name = "contentInputs";
            this.contentInputs.Size = new System.Drawing.Size(433, 327);
            this.contentInputs.TabIndex = 1;
            this.contentInputs.Paint += new System.Windows.Forms.PaintEventHandler(this.contentInputs_Paint);
            // 
            // contentControlsButtonLogin
            // 
            this.contentControlsButtonLogin.Controls.Add(this.panelDB8);
            this.contentControlsButtonLogin.Controls.Add(this.panelDB9);
            this.contentControlsButtonLogin.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentControlsButtonLogin.Location = new System.Drawing.Point(0, 148);
            this.contentControlsButtonLogin.Name = "contentControlsButtonLogin";
            this.contentControlsButtonLogin.Padding = new System.Windows.Forms.Padding(70, 30, 70, 0);
            this.contentControlsButtonLogin.Size = new System.Drawing.Size(433, 109);
            this.contentControlsButtonLogin.TabIndex = 2;
            // 
            // panelDB8
            // 
            this.panelDB8.AutoSize = true;
            this.panelDB8.Controls.Add(this.btnLogin);
            this.panelDB8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDB8.Location = new System.Drawing.Point(70, 45);
            this.panelDB8.Name = "panelDB8";
            this.panelDB8.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panelDB8.Size = new System.Drawing.Size(293, 53);
            this.panelDB8.TabIndex = 4;
            // 
            // btnLogin
            // 
            this.btnLogin.AutoSize = true;
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(18)))));
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Bahnschrift Condensed", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(0, 10);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(293, 33);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "INICIAR SESIÓN";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // panelDB9
            // 
            this.panelDB9.AutoSize = true;
            this.panelDB9.Controls.Add(this.mensajeErrorLabel);
            this.panelDB9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDB9.Location = new System.Drawing.Point(70, 30);
            this.panelDB9.Name = "panelDB9";
            this.panelDB9.Padding = new System.Windows.Forms.Padding(70, 15, 70, 0);
            this.panelDB9.Size = new System.Drawing.Size(293, 15);
            this.panelDB9.TabIndex = 3;
            // 
            // mensajeErrorLabel
            // 
            this.mensajeErrorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.mensajeErrorLabel.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.mensajeErrorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mensajeErrorLabel.Location = new System.Drawing.Point(70, 15);
            this.mensajeErrorLabel.Name = "mensajeErrorLabel";
            this.mensajeErrorLabel.Size = new System.Drawing.Size(153, 0);
            this.mensajeErrorLabel.TabIndex = 0;
            this.mensajeErrorLabel.Text = "CONTRASEÑA";
            this.mensajeErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contentControlsContraseña
            // 
            this.contentControlsContraseña.AutoSize = true;
            this.contentControlsContraseña.Controls.Add(this.panelDB5);
            this.contentControlsContraseña.Controls.Add(this.panelDB6);
            this.contentControlsContraseña.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentControlsContraseña.Location = new System.Drawing.Point(0, 79);
            this.contentControlsContraseña.Name = "contentControlsContraseña";
            this.contentControlsContraseña.Size = new System.Drawing.Size(433, 69);
            this.contentControlsContraseña.TabIndex = 1;
            // 
            // panelDB5
            // 
            this.panelDB5.AutoSize = true;
            this.panelDB5.Controls.Add(this.inputContraseña);
            this.panelDB5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDB5.Location = new System.Drawing.Point(0, 33);
            this.panelDB5.Name = "panelDB5";
            this.panelDB5.Padding = new System.Windows.Forms.Padding(70, 10, 70, 0);
            this.panelDB5.Size = new System.Drawing.Size(433, 36);
            this.panelDB5.TabIndex = 2;
            // 
            // inputContraseña
            // 
            this.inputContraseña.Dock = System.Windows.Forms.DockStyle.Top;
            this.inputContraseña.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.inputContraseña.Location = new System.Drawing.Point(70, 10);
            this.inputContraseña.Name = "inputContraseña";
            this.inputContraseña.PasswordChar = '*';
            this.inputContraseña.Size = new System.Drawing.Size(293, 26);
            this.inputContraseña.TabIndex = 0;
            this.inputContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.inputContraseña.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputContraseña_KeyPress);
            // 
            // panelDB6
            // 
            this.panelDB6.AutoSize = true;
            this.panelDB6.Controls.Add(this.contraseñaLabel);
            this.panelDB6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDB6.Location = new System.Drawing.Point(0, 0);
            this.panelDB6.Name = "panelDB6";
            this.panelDB6.Padding = new System.Windows.Forms.Padding(70, 15, 70, 0);
            this.panelDB6.Size = new System.Drawing.Size(433, 33);
            this.panelDB6.TabIndex = 3;
            // 
            // contraseñaLabel
            // 
            this.contraseñaLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.contraseñaLabel.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.contraseñaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.contraseñaLabel.Location = new System.Drawing.Point(70, 15);
            this.contraseñaLabel.Name = "contraseñaLabel";
            this.contraseñaLabel.Size = new System.Drawing.Size(293, 18);
            this.contraseñaLabel.TabIndex = 0;
            this.contraseñaLabel.Text = "CONTRASEÑA";
            this.contraseñaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.contraseñaLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // contentControlsNombreUsuario
            // 
            this.contentControlsNombreUsuario.AutoSize = true;
            this.contentControlsNombreUsuario.Controls.Add(this.contentInputNombre);
            this.contentControlsNombreUsuario.Controls.Add(this.contentLabelNombreU);
            this.contentControlsNombreUsuario.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentControlsNombreUsuario.Location = new System.Drawing.Point(0, 0);
            this.contentControlsNombreUsuario.Name = "contentControlsNombreUsuario";
            this.contentControlsNombreUsuario.Size = new System.Drawing.Size(433, 79);
            this.contentControlsNombreUsuario.TabIndex = 0;
            this.contentControlsNombreUsuario.Paint += new System.Windows.Forms.PaintEventHandler(this.contentControlsNameUser_Paint);
            // 
            // contentInputNombre
            // 
            this.contentInputNombre.AutoSize = true;
            this.contentInputNombre.Controls.Add(this.inputNombreUsuario);
            this.contentInputNombre.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentInputNombre.Location = new System.Drawing.Point(0, 43);
            this.contentInputNombre.Name = "contentInputNombre";
            this.contentInputNombre.Padding = new System.Windows.Forms.Padding(70, 10, 70, 0);
            this.contentInputNombre.Size = new System.Drawing.Size(433, 36);
            this.contentInputNombre.TabIndex = 2;
            // 
            // inputNombreUsuario
            // 
            this.inputNombreUsuario.Dock = System.Windows.Forms.DockStyle.Top;
            this.inputNombreUsuario.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.inputNombreUsuario.Location = new System.Drawing.Point(70, 10);
            this.inputNombreUsuario.Name = "inputNombreUsuario";
            this.inputNombreUsuario.Size = new System.Drawing.Size(293, 26);
            this.inputNombreUsuario.TabIndex = 0;
            this.inputNombreUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.inputNombreUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputNombreUsuario_KeyPress);
            // 
            // contentLabelNombreU
            // 
            this.contentLabelNombreU.AutoSize = true;
            this.contentLabelNombreU.Controls.Add(this.nombreUsuarioLabel);
            this.contentLabelNombreU.Dock = System.Windows.Forms.DockStyle.Top;
            this.contentLabelNombreU.Location = new System.Drawing.Point(0, 0);
            this.contentLabelNombreU.Name = "contentLabelNombreU";
            this.contentLabelNombreU.Padding = new System.Windows.Forms.Padding(70, 25, 70, 0);
            this.contentLabelNombreU.Size = new System.Drawing.Size(433, 43);
            this.contentLabelNombreU.TabIndex = 3;
            // 
            // nombreUsuarioLabel
            // 
            this.nombreUsuarioLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nombreUsuarioLabel.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F);
            this.nombreUsuarioLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.nombreUsuarioLabel.Location = new System.Drawing.Point(70, 25);
            this.nombreUsuarioLabel.Name = "nombreUsuarioLabel";
            this.nombreUsuarioLabel.Size = new System.Drawing.Size(293, 18);
            this.nombreUsuarioLabel.TabIndex = 0;
            this.nombreUsuarioLabel.Text = "NOMBRE DE USUARIO";
            this.nombreUsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelDB2
            // 
            this.panelDB2.Controls.Add(this.panelDB3);
            this.panelDB2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDB2.Location = new System.Drawing.Point(0, 0);
            this.panelDB2.Name = "panelDB2";
            this.panelDB2.Size = new System.Drawing.Size(433, 100);
            this.panelDB2.TabIndex = 0;
            // 
            // panelDB3
            // 
            this.panelDB3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(92)))), ((int)(((byte)(138)))));
            this.panelDB3.Controls.Add(this.label4);
            this.panelDB3.Controls.Add(this.label3);
            this.panelDB3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDB3.Location = new System.Drawing.Point(0, 0);
            this.panelDB3.Name = "panelDB3";
            this.panelDB3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panelDB3.Size = new System.Drawing.Size(433, 100);
            this.panelDB3.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(433, 47);
            this.label4.TabIndex = 1;
            this.label4.Text = "CASINO EL MONJE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(433, 33);
            this.label3.TabIndex = 0;
            this.label3.Text = "SISTEMA DE GESTIÓN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 427);
            this.Controls.Add(this.panelDB1);
            this.DoubleBuffered = true;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.panelDB1.ResumeLayout(false);
            this.contentInputs.ResumeLayout(false);
            this.contentInputs.PerformLayout();
            this.contentControlsButtonLogin.ResumeLayout(false);
            this.contentControlsButtonLogin.PerformLayout();
            this.panelDB8.ResumeLayout(false);
            this.panelDB8.PerformLayout();
            this.panelDB9.ResumeLayout(false);
            this.contentControlsContraseña.ResumeLayout(false);
            this.contentControlsContraseña.PerformLayout();
            this.panelDB5.ResumeLayout(false);
            this.panelDB5.PerformLayout();
            this.panelDB6.ResumeLayout(false);
            this.contentControlsNombreUsuario.ResumeLayout(false);
            this.contentControlsNombreUsuario.PerformLayout();
            this.contentInputNombre.ResumeLayout(false);
            this.contentInputNombre.PerformLayout();
            this.contentLabelNombreU.ResumeLayout(false);
            this.panelDB2.ResumeLayout(false);
            this.panelDB3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CVistas.PanelDB panelDB1;
        private CVistas.PanelDB panelDB2;
        private CVistas.PanelDB panelDB3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private CVistas.PanelDB contentInputs;
        private CVistas.PanelDB contentControlsNombreUsuario;
        private CVistas.PanelDB contentInputNombre;
        private System.Windows.Forms.TextBox inputNombreUsuario;
        private CVistas.PanelDB contentLabelNombreU;
        private System.Windows.Forms.Label nombreUsuarioLabel;
        private CVistas.PanelDB contentControlsContraseña;
        private CVistas.PanelDB panelDB5;
        private System.Windows.Forms.TextBox inputContraseña;
        private CVistas.PanelDB panelDB6;
        private System.Windows.Forms.Label contraseñaLabel;
        private CVistas.PanelDB contentControlsButtonLogin;
        private CVistas.PanelDB panelDB8;
        private System.Windows.Forms.Button btnLogin;
        private CVistas.PanelDB panelDB9;
        private System.Windows.Forms.Label mensajeErrorLabel;
    }
}